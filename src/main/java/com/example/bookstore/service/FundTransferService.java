package com.example.bookstore.service;

import com.example.bookstore.models.request.Amount;
import com.example.bookstore.models.request.FundsTransferRequest;
import com.example.bookstore.models.request.TransferFundsRequest;
import com.example.bookstore.models.response.FundTransferResponse;
import com.google.gson.Gson;
import okhttp3.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

@Service
public class FundTransferService {

    private final String fundsTransferApiPath;
    private final String fusionBasePath;
    private final String fusionIfiID;
    private final String fusionAuthToken;
    private final String fundsTransferCode;
    private final String fundingAccountId;
    private final Gson gson;
    private final OkHttpClient httpClient;

    public FundTransferService(@Value("${fusion.fund.transfer}") String fundsTransferApiPath,
                               @Value("${fusion.base.path}") String fusionBasePath,
                               @Value("${fusion.ifiId}") String fusionifiID,
                               @Value("${fusion.authToken}") String fusionAuthToken,
                               @Value("${fusion.fund.transfer.code}") String fundsTransferCode,
                               @Value("${fusion.self.funding.account}") String fundingAccountId,
                               @Value("${fusion.self.merchant.account}") String merchantAccountId,
                               Gson gson,
                               OkHttpClient httpClient) {
        this.fundsTransferApiPath = fundsTransferApiPath;
        this.fusionBasePath = fusionBasePath;
        this.fusionIfiID = fusionifiID;
        this.fusionAuthToken = fusionAuthToken;
        this.fundsTransferCode = fundsTransferCode;
        this.fundingAccountId = fundingAccountId;
        this.gson = gson;
        this.httpClient = httpClient;
    }

    public Request fundsTransferRequestToMerchant(double amount, String fromAccId, String toAccId){
        return new Request.Builder()
                .url(fundsTransferApiPath(toAccId))
                .post(getFundsTransferRequestBodyToMerchant(amount, fromAccId, toAccId))
                .header("X-Zeta-AuthToken",fusionAuthToken)
                .build();
    }

    public RequestBody getFundsTransferRequestBodyToMerchant(double amount, String fromAccId, String toAccId){
        FundsTransferRequest req =  FundsTransferRequest.builder()
                .requestID(UUID.randomUUID().toString())
                .amount(Amount.builder().amount(amount).currency("INR").build())
                .transferCode(fundsTransferCode)
                .debitAccountID(fromAccId)
                .creditAccountID(toAccId)
                .transferTime(System.currentTimeMillis())
                .remarks("New account cashback")
                .attributes(null)
                .build();
        return RequestBody.create(gson.toJson(req), MediaType.parse("application/json"));
    }

    public CompletionStage<Boolean> sendFunds(int amount, String fromAccId, String toAccId) {
        System.out.println(fromAccId);
        System.out.println(toAccId);
        Request request = fundsTransferRequest(amount, fromAccId);
        Response response=null;
        try {
            response = httpClient.newCall(request).execute();
            gson.fromJson(response.body().string(),FundTransferResponse.class);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Funds Transfer Failed");
        }

        request = fundsTransferRequestToMerchant(amount, fromAccId, toAccId);
        response=null;
        try {
            response = httpClient.newCall(request).execute();
            gson.fromJson(response.body().string(),FundTransferResponse.class);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Funds Transfer Failed");
        }
        return CompletableFuture.completedFuture(true);
    }

    public FundTransferResponse sendFundstoMerchant(double amount, String accountID) {
        Request request = fundsTransferRequest(amount,accountID);
        Response response=null;
        try {
            response = httpClient.newCall(request).execute();
            return gson.fromJson(response.body().string(),FundTransferResponse.class);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Funds Transfer Failed");
        }
    }
    public FundTransferResponse sendFunds(double amount, String accountID) {
        Request request = fundsTransferRequest(amount,accountID);
        Response response=null;
        try {
            response = httpClient.newCall(request).execute();
            return gson.fromJson(response.body().string(),FundTransferResponse.class);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Funds Transfer Failed");
        }
    }

    public String fundsTransferApiPath(String accountId){
       return UriComponentsBuilder.fromHttpUrl(fusionBasePath+fundsTransferApiPath).buildAndExpand(fusionIfiID,accountId).toString();
    }

    public Request fundsTransferRequest(double amount, String accountID){
        return new Request.Builder()
                .url(fundsTransferApiPath(accountID))
                .post(getFundsTransferRequestBody(amount,accountID))
                .header("X-Zeta-AuthToken",fusionAuthToken)
                .build();
    }

    public RequestBody getFundsTransferRequestBody(double amount, String accountID){
        FundsTransferRequest req =  FundsTransferRequest.builder()
                .requestID(UUID.randomUUID().toString())
                .amount(Amount.builder().amount(amount).currency("INR").build())
                .transferCode(fundsTransferCode)
                .debitAccountID(fundingAccountId)
                .creditAccountID(accountID)
                .transferTime(System.currentTimeMillis())
                .remarks("New account cashback")
                .attributes(null)
                .build();
        return RequestBody.create(gson.toJson(req), MediaType.parse("application/json"));
    }


//    public CompletionStage<Object> sendFunds(TransferFundsRequest transferFundsRequest, String fromAccId, String toAccId) {
//    }
}
