package com.example.bookstore.service;

import com.example.bookstore.exception.AuthorizationFailureException;
import com.example.bookstore.exception.LimitExceedException;
import com.example.bookstore.exception.NotFoundException;
import com.example.bookstore.models.*;


import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

import com.example.bookstore.models.request.TransferFundsRequest;
import com.example.bookstore.models.response.*;
import com.google.gson.Gson;
import okhttp3.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

@Service
public class AccountHolderService {
    private final OkHttpClient httpClient;
    private final AuthorizationService authorizationService;
    private final FundTransferService fundTransferService;
    private final String fusionBasePath;
    private final String fusionIfiID;
    private final String fusionCreateAH;
    private final String fusionAuthToken;
    private final String issueBundlePath;
    private final String minKycBundle;
    private final String getResource;
    private final String getCard;
    private final String allAccountsApiPath;
    private final String fusionGetBalanceApi;
    private final String getAllTransactionsApiPath;
    private final Gson gson;
    private static final int MIN_LIMIT = 5000;
    private static final String MIN_KYC_STATUS = "MINIMAL";
    private final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");

    @Autowired
    public AccountHolderService(OkHttpClient httpClient, AuthorizationService authorizationService,
                                FundTransferService fundTransferService,
                                @Value("${fusion.base.path}") String fusionBasePath,
                                @Value("${fusion.ifiId}") String fusionIfiID,
                                @Value("${fusion.createAH}") String fusionCreateAH,
                                @Value("${fusion.authToken}") String fusionAuthToken,
                                @Value("${fusion.issueBundle}") String issueBundlePath,
                                @Value("${fusion.minKyc.bundleId}") String minKycBundle,
                                @Value("${fusion.get.resource}") String getResource,
                                @Value("${fusion.get.card}") String getCard,
                                @Value("${fusion.accountholder.all.accounts.api.path}") String allAccountsApiPath,
                                @Value("${fusion.account.balance.api}") String fusionGetBalanceApi,
                                @Value("${fusion.account.all.transactions}") String getAllTransactionsApiPath, Gson gson) {
        this.httpClient = httpClient;
        this.authorizationService = authorizationService;
        this.fundTransferService = fundTransferService;
        this.fusionIfiID = fusionIfiID;
        this.fusionAuthToken = fusionAuthToken;
        this.fusionBasePath = fusionBasePath;
        this.fusionCreateAH = fusionCreateAH;
        this.issueBundlePath = issueBundlePath;
        this.minKycBundle = minKycBundle;
        this.getResource = getResource;
        this.getCard = getCard;
        this.allAccountsApiPath = allAccountsApiPath;
        this.fusionGetBalanceApi = fusionGetBalanceApi;
        this.getAllTransactionsApiPath = getAllTransactionsApiPath;
        this.gson = gson;
    }

    public CompletionStage<AccountHolderCreationResponse> createMinKYCAccountHolder(CreateMinKycAccountHolderRequest createAccountHolderRequest) {
        return authorizationService.checkUserIsAlreadyPresent(createAccountHolderRequest.getPhoneNumber())
                .thenApply(__ -> {
            FusionAccountHolderCreationResponse ahResponse = performFusionCreateAhRequest(createAccountHolderRequest);
            String accountHolderId = ahResponse.getIndividualID();
            issueBundle(createAccountHolderRequest, accountHolderId);
            return accountHolderId;
        }).thenCompose(accountHolderId -> authorizationService.addAuthorization(createAccountHolderRequest.getPhoneNumber(), createAccountHolderRequest.getPassword(),
                accountHolderId, MIN_LIMIT, createAccountHolderRequest.getName(), createAccountHolderRequest.getProfilePicURL())
                .thenApply(__ -> AccountHolderCreationResponse.builder().accountHolderId(accountHolderId)
                        .limit(MIN_LIMIT)
                        .name(createAccountHolderRequest.getName())
                        .profilePicURL(createAccountHolderRequest.getProfilePicURL())
                        .maxLimit(5000)
                        .build()));
    }

    private FusionAccountHolderCreationResponse performFusionCreateAhRequest(CreateMinKycAccountHolderRequest createAccountHolderRequest) {
        Request request = new Request.Builder()
                .url(getCreateAHPath())
                .header("X-Zeta-AuthToken", fusionAuthToken)
                .post(createAhReqBody(createAccountHolderRequest.getName(),
                        createAccountHolderRequest.getDob(),
                        createAccountHolderRequest.getPhoneNumber(),
                        createAccountHolderRequest.getPan(), createAccountHolderRequest.getProfilePicURL(), createAccountHolderRequest.getCustomFields())).build();
        try {
            Response response = httpClient.newCall(request).execute();
            return gson.fromJson(response.body().string(), FusionAccountHolderCreationResponse.class);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("AccountHolder Creation Failed :" + e.getMessage());
        }
    }

    private IssueBundleResponse issueBundle(CreateMinKycAccountHolderRequest request, String accountHolderId) {
        try {
            Request request2 = new Request.Builder()
                    .url(getIssueBundlePath())
                    .header("X-Zeta-AuthToken", fusionAuthToken)
                    .post(issueBundleRequestBody(request.getPhoneNumber(), accountHolderId)).build();
            Response response = httpClient.newCall(request2).execute();
            return gson.fromJson(response.body().string(), IssueBundleResponse.class);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Bundle Issuance Failed :" + e.getMessage());
        }
    }

    private String getCreateAHPath() {
        return UriComponentsBuilder.fromHttpUrl(fusionBasePath + fusionCreateAH).buildAndExpand(fusionIfiID).toString();
    }

    private RequestBody createAhReqBody(String name, String dob, String phoneNumber, String pan, String profilePicURL, CustomFields customFields) {
        FusionCreateAccountHolderRequest req = FusionCreateAccountHolderRequest.builder()
                .firstName(name)
                .dob(LocalDate.parse(dob, dateFormatter))
                .ifiID(Integer.valueOf(fusionIfiID))
                .formID(UUID.randomUUID().toString())
                .vectors(Arrays.asList(VectorsItem.builder().type("p").value(phoneNumber).build()))
                .kycDetails(KycDetails.builder()
                        .kycStatus(MIN_KYC_STATUS)
                        .kycStatusPostExpiry(MIN_KYC_STATUS)
                        .authType("PAN")
                        .authData(Collections.singletonMap("PAN", pan))
                        .build())
                .profilePicURL(profilePicURL)
                .customFields(customFields)
                .build();
        return RequestBody.create(gson.toJson(req), MediaType.parse("application/json"));
    }

    private String getIssueBundlePath() {
        return UriComponentsBuilder.fromHttpUrl(fusionBasePath + issueBundlePath).buildAndExpand(fusionIfiID, minKycBundle).toString();
    }

    private RequestBody issueBundleRequestBody(String phoneNumber, String accountholderId) {
        //Issue Bundle Code
        IssueBundleReq issueBundleReq = IssueBundleReq.builder()
                .accountHolderID(accountholderId)
                .name("MinKYCBundle")
                .phoneNumber(phoneNumber)
                .build();
        return RequestBody.create(gson.toJson(issueBundleReq), MediaType.parse("application/json"));
    }

    public CompletionStage<AccountHolderCreationResponse> login(LoginRequest loginRequest) {
        return authorizationService.checkUserIsPresent(loginRequest.getPhoneNumber())
                .thenApply(authorization -> {
                    if(authorization.getPassword().equals(loginRequest.getPassword())) {

                        throw new AuthorizationFailureException("Authorization Failed : Incorrect Password");
                    }
                    return getAccountHolderView(authorization);
                });
    }

    private AccountHolderCreationResponse getAccountHolderView(Authorization authorization) {
        return AccountHolderCreationResponse.builder()
                .accountHolderId(authorization.getAccountHolderId())
                .profilePicURL(authorization.getProfilePicURL())
                .limit(authorization.getMoney_limit())
                .name(authorization.getUsername())
                .maxLimit(5000)
                .build();
    }

    public CompletionStage<CardViewResponse> getCardView(String accountHolderId) {
        return CompletableFuture.supplyAsync(() -> {
            Resource resource = performFusionGetResource(accountHolderId);
            FormFactorProduct formFactorProduct = resource.getResourceProduct().getFormFactorProducts().stream().filter(e -> "card".equals(e.getType())).findFirst().orElseThrow(() -> new NotFoundException("Could not find card product"));
            return resource.getFormFactors().stream().filter(e -> e.getFormFactorProductID().equals(formFactorProduct.getId())).findFirst().orElseThrow(() -> new NotFoundException("Could not find card product"));
        }).thenApply(formFactor -> {
            String cardId = formFactor.getFormFactorID();
            return performFusionGetCard(cardId);
        });
    }


    private Resource performFusionGetResource(String accountHolderId) {
        Map<String, String> params = new HashMap<>();
        params.put("vectorType", "ACCOUNTHOLDER");
        params.put("vectorValue", accountHolderId);

        HttpUrl httpUrl = getURL(getResourceURL(), params);

        Request request = new Request.Builder()
                .url(httpUrl)
                .header("X-Zeta-AuthToken", fusionAuthToken)
                .get().build();
        try {
            Response response = httpClient.newCall(request).execute();
            return gson.fromJson(response.body().string(), Resource.class);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("AccountHolder Creation Failed :" + e.getMessage());
        }
    }

    private CardViewResponse performFusionGetCard(String cardId) {
        Request request = new Request.Builder()
                .url(getCardViewURL(cardId))
                .header("X-Zeta-AuthToken", fusionAuthToken)
                .get().build();
        try {
            Response response = httpClient.newCall(request).execute();
            return gson.fromJson(response.body().string(), CardViewResponse.class);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("AccountHolder Creation Failed :" + e.getMessage());
        }
    }

    private String getResourceURL() {
        return UriComponentsBuilder.fromHttpUrl(fusionBasePath + getResource).buildAndExpand(fusionIfiID).toString();
    }

    private String getCardViewURL(String cardId) {
        return UriComponentsBuilder.fromHttpUrl(fusionBasePath + getCard).buildAndExpand(fusionIfiID, cardId).toString();
    }

    private HttpUrl getURL(String url, Map<String,String> params) {
        HttpUrl.Builder httpBuilder = HttpUrl.parse(url).newBuilder();
        if (params != null) {
            for(Map.Entry<String, String> param : params.entrySet()) {
                httpBuilder.addQueryParameter(param.getKey(),param.getValue());
            }
        }
        return httpBuilder.build();
    }

    public CompletionStage<AccountHolderCreationResponse> transferMoney(TransferFundsRequest transferFundsRequest) {
        return checkIfTransactionWithinLimit(transferFundsRequest.getFromAccountHolderId(), transferFundsRequest.getAmount())
                .thenCompose(newLimit -> findAccountId(transferFundsRequest.getFromAccountHolderId())
                        .thenCompose(fromAccId -> findAccountId(transferFundsRequest.getToAccountHolderId())
                                .thenCompose(toAccId -> fundTransferService.sendFunds(transferFundsRequest.getAmount(), fromAccId, toAccId)
                                        .thenCompose(__ -> setLimitForAccountHolder(transferFundsRequest.getFromAccountHolderId(), newLimit)))));
    }

    private CompletionStage<AccountHolderCreationResponse> setLimitForAccountHolder(String fromAccountHolderId, int newLimit) {
        return authorizationService.setLimitForAccountHolder(fromAccountHolderId, newLimit)
                .thenApply(authorization -> getAccountHolderView(authorization));
    }

    private CompletionStage<Integer> checkIfTransactionWithinLimit(String fromAccountHolderId, int amount) {
        return authorizationService.fetchAuthorizationForAccountHolder(fromAccountHolderId)
                .thenApply(authorization -> {
                  int limit = authorization.getMoney_limit();
                  if(amount > limit)
                      throw new LimitExceedException("Exceeded Limit Balance for this Month");
                  return limit-amount;
                });
    }

    private CompletableFuture<String> findAccountId(String fromAccountHolderId) {
        AllAccountHolderAccounts allAccountHolderAccounts = getAllAccountHolderAcconts(fromAccountHolderId);
        return  CompletableFuture.completedFuture(allAccountHolderAccounts.getAccounts().get(0).getId());
    }

    public CompletionStage<Balance> fetchAllAccountsBalance(String accountHolderId) {
        return CompletableFuture.completedFuture(fetchBalanceOfAllAccounts(getAllAccountHolderAcconts(accountHolderId)));
    }

    private Balance fetchBalanceOfAllAccounts(AllAccountHolderAccounts allAccountHolderAccounts) {
        double balance = 0;
        for (AccountSheet accountSheet : allAccountHolderAccounts.getAccounts()) {
            balance += getBalanceOfASingleAccount(accountSheet.getId());
        }
        return Balance.builder().balance(balance).build();
    }

    private double getBalanceOfASingleAccount(String accountId) {
        Request request = new Request.Builder()
                .url(getAaccountBalanceApiPath(accountId))
                .header("X-Zeta-AuthToken", fusionAuthToken)
                .build();

        AccountBalance accountBalance = null;

        try {
            Response response = httpClient.newCall(request).execute();
            accountBalance = gson.fromJson(response.body().string(), AccountBalance.class);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to Fetch Balance");

        }
        return accountBalance.getBalance();
    }

    private String getAaccountBalanceApiPath(String accountId) {
        return UriComponentsBuilder.fromHttpUrl(fusionBasePath + fusionGetBalanceApi).buildAndExpand(fusionIfiID, accountId).toString();
    }

    public AllAccountHolderAccounts getAllAccountHolderAcconts(String accountHolderId) {
        Request request = new Request.Builder()
                .url(getAllAccountsApiPath(accountHolderId))
                .header("X-Zeta-AuthToken", fusionAuthToken)
                .build();

        AllAccountHolderAccounts allAccountHolderAccounts = null;
        try {
            Response response = httpClient.newCall(request).execute();
            allAccountHolderAccounts = gson.fromJson(response.body().string(), AllAccountHolderAccounts.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return allAccountHolderAccounts;
    }

    private String getAllAccountsApiPath(String accountHolderId) {
        UriComponents uri = UriComponentsBuilder
                .fromHttpUrl(fusionBasePath + allAccountsApiPath)
                .buildAndExpand(fusionIfiID, accountHolderId);
        return uri.toString();
    }

    public TransactionList getAllTransactionsForSingleAccount(String accountholderID) {
        AllAccountHolderAccounts allAccounts = getAllAccountHolderAcconts(accountholderID);
        return getAllTransactions(allAccounts.getAccounts().get(0).getId(), "1", "10");
    }

    public TransactionList getAllTransactions(String accountId, String pageNumber, String pageSize) {
        HttpUrl.Builder httpBuilder = HttpUrl.parse(getAllTransactionsPath(accountId)).newBuilder();
        httpBuilder.addQueryParameter("pageNumber", pageNumber);
        httpBuilder.addQueryParameter("pageSize", pageSize);

        Request request = new Request.Builder()
                .url(httpBuilder.build())
                .header("X-Zeta-AuthToken", fusionAuthToken)
                .build();

        TransactionList transactionList = null;
        try {
            Response response = httpClient.newCall(request).execute();
            transactionList = gson.fromJson(response.body().string(), TransactionList.class);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException("Unable to Fetch Transactions");
        }

        return transactionList;
    }

    private String getAllTransactionsPath(String accountId) {
        return UriComponentsBuilder
                .fromHttpUrl(fusionBasePath + getAllTransactionsApiPath)
                .buildAndExpand(fusionIfiID, accountId).toString();
    }
}
