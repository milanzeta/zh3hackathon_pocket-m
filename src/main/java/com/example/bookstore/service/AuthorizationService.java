package com.example.bookstore.service;

import com.example.bookstore.dao.AuthorizationDao;
import com.example.bookstore.exception.NotFoundException;
import com.example.bookstore.exception.UserAlreadyPresentException;
import com.example.bookstore.models.AccountHolderCreationResponse;
import com.example.bookstore.models.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.CompletionStage;

@Service
public class AuthorizationService {
    private final AuthorizationDao authorizationDao;

    @Autowired
    public AuthorizationService(AuthorizationDao authorizationDao) {
        this.authorizationDao = authorizationDao;
    }

    public CompletionStage<Boolean> addAuthorization(String phoneNumber, String password, String accountHolderId, int limit, String username, String profilepicurl) {
        return authorizationDao.addAuthorization(phoneNumber, password, accountHolderId, limit, username, profilepicurl);
    }

    public CompletionStage<Authorization> checkUserIsPresent(String phoneNumber) {
        return authorizationDao.checkUserIsPresent(phoneNumber)
                .exceptionally(th -> {
                    throw new NotFoundException("Incorrect phoneNumber used to login");
                });
    }

    public CompletionStage<Boolean> checkUserIsAlreadyPresent(String phoneNumber) {
        return authorizationDao.checkUserIsAlreadyPresent(phoneNumber)
                .thenApply(aBoolean -> {
                    if(aBoolean) {
                        throw new UserAlreadyPresentException("User is already present");
                    }
                    return aBoolean;
                });

    }

    public CompletionStage<Authorization> fetchAuthorizationForAccountHolder(String accountHolderId) {
        return authorizationDao.fetchAuthorizationForAccountHolder(accountHolderId);
    }

    public CompletionStage<Authorization> setLimitForAccountHolder(String accountHolderId, int limit) {
        return authorizationDao.setLimitForAccountHolder(accountHolderId, limit)
                .thenCompose(__ -> fetchAuthorizationForAccountHolder(accountHolderId));
    }
}
