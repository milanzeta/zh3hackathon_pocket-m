package com.example.bookstore.dao;

import com.example.bookstore.models.AccountHolderCreationResponse;
import com.example.bookstore.models.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

@Repository
public class AuthorizationDao {

    private static final String CHECK_USER = "SELECT * from auth where phonenumber = ?";
    private static final String CHECK_AH = "SELECT * from auth where accountholderid = ?";
    private static final String SET_AH = "UPDATE auth set money_limit = ? where accountholderid = ?";
    private final JdbcTemplate jdbcTemplate;
    private static final String INSERT_COLUMNS = "INSERT INTO auth " +
            "(phonenumber, password, accountholderid, money_limit, username, profilepicurl) " +
            "values(?, ?, ?, ?, ?, ?)";

    private final RowMapper<Authorization> authorizationRowMapper =
            (rs, i) -> Authorization.builder()
                    .phoneNumber(rs.getString("phonenumber"))
                    .password(rs.getString("password"))
            .accountHolderId(rs.getString("accountholderid"))
            .money_limit(rs.getInt("money_limit"))
            .username(rs.getString("username"))
            .profilePicURL(rs.getString("profilepicurl")).build();

    @Autowired
    public AuthorizationDao(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public CompletionStage<Boolean> addAuthorization(String phoneNumber, String password, String accountHolderId, Integer limit, String username, String profilepicurl) {
        return CompletableFuture.supplyAsync(() ->  jdbcTemplate.update(INSERT_COLUMNS, phoneNumber, password, accountHolderId, limit, username, profilepicurl))
                .thenApply(count -> count==1);
    }

    public CompletionStage<Authorization> checkUserIsPresent(String phoneNumber) {
        return CompletableFuture.supplyAsync(() -> jdbcTemplate.queryForObject(CHECK_USER, new Object[]{phoneNumber}, authorizationRowMapper));
    }


    public CompletionStage<Boolean> checkUserIsAlreadyPresent(String phoneNumber) {
        return CompletableFuture.supplyAsync(() -> jdbcTemplate.queryForObject(CHECK_USER, new Object[]{phoneNumber}, authorizationRowMapper))
                .thenApply(__ -> true)
                .exceptionally(th -> false);
    }

    public CompletionStage<Authorization> fetchAuthorizationForAccountHolder(String accountHolderId) {
        return CompletableFuture.supplyAsync(() -> jdbcTemplate.queryForObject(CHECK_AH, new Object[]{accountHolderId}, authorizationRowMapper));
    }

    public CompletionStage<Boolean> setLimitForAccountHolder(String accountHolderId, int limit) {
        return CompletableFuture.supplyAsync(() -> jdbcTemplate.update(SET_AH, limit, accountHolderId))
                .thenApply(cnt -> cnt==1);
    }
}
