package com.example.bookstore.models;

import com.google.gson.JsonElement;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;


@Data
@Builder
public class FusionCreateAccountHolderRequest {
	private String formID;
	private String lastName;
	private int ifiID;
	private String firstName;
	private List<VectorsItem> vectors;
	private LocalDate dob;
	private KycDetails kycDetails;
	private String profilePicURL;
	private CustomFields customFields;

}