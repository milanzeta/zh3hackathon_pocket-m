package com.example.bookstore.models;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Builder
@Data
public class Policies {
    List<String> issuancePolicies;
    List<String> paymentPolicies;
}
