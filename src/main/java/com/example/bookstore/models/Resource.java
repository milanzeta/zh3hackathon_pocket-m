package com.example.bookstore.models;

import lombok.Builder;
import lombok.Data;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Builder
@Data
public class Resource extends ZetaPayload {
    private Long ifi;
    private String id;
    private String resourceProductId;
    private ResourceProduct resourceProduct;
    private String targetURI;
    private List<FormFactor> formFactors;
    private String authProfileId;
    private List<String> tags;
    private List<String> vectors;
    private Policies policies;
    private Map<String, Object> attributes;
    private Status status;
    private Date createdAt;
    private Date modifiedAt;
}