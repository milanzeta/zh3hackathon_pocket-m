package com.example.bookstore.models;

import java.util.Map;

public class AccountV2 {
    private String id;
    private Map<String, String> attributes;

    public AccountV2(String id, Map<String, String> attributes) {
        this.id = id;
        this.attributes = attributes;
    }

    public String getId() {
        return id;
    }

    public Map<String, String> getAttributes() {
        return attributes;
    }
}

