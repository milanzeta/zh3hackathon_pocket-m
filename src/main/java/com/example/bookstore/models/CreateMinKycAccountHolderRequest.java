package com.example.bookstore.models;

import com.google.gson.JsonElement;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CreateMinKycAccountHolderRequest {
    String password;
    String name;
    String dob;
    String phoneNumber;
    String pan;
    String profilePicURL;
    CustomFields customFields;
}
