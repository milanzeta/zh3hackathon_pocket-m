package com.example.bookstore.models.response;

import lombok.Data;

import java.util.Map;

@Data
public class AccountBalance {
	private Map<String,String> headers;
	private double balance;
	private String lastTransactionID;
	private String accountingType;
	private String currency;
}
