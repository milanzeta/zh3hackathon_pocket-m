package com.example.bookstore.models.response;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class FundTransferResponse {
    private String requestID;
    private String transferID;
    private String message;
    private String status;
    private String isRetriableFailure;
}
