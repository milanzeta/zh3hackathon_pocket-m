package com.example.bookstore.models.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class AccountTransactionListItem {
	private Map<String,String> headers;
	private int amount;
	private String recordType;
	private int newBalance;
	private String transactionID;
	private int previousBalance;
	private String accountID;
	private String postingID;
	private String currency;
	private Attributes attributes;
	private List<Object> reversalTransactionIDs;
	private String remarks;
	private long timestamp;
}