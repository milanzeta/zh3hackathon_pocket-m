package com.example.bookstore.models.response;

import lombok.Builder;
import lombok.Data;

import java.sql.Timestamp;
import java.util.Map;

@Builder
@Data
public class AccountRelation {
    private final String id;
    private final Long ifiID;
    private final String accountID;
    private final String relatedAccountID;
    private final String relationshipType;
    private final String status;
    private final Map<String, String> attributes;
    private final Timestamp createdAt;
    private final Timestamp updatedAt;
}
