package com.example.bookstore.models.response;


import lombok.Data;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

@Data
public class AccountVector {
    private final String id;
    private final long ifiID;
    private final String accountID;
    private final String value;
    private final String type;
    private final String status;
    private final Map<String, String> attributes;
    private final Timestamp createdAt;
    private final Timestamp updatedAt;
    private final List<Tag> tags;
}