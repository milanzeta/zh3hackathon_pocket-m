package com.example.bookstore.models.response;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@Data
@Builder
public class AccountSheet {
    private final String id;
    private final List<AccountVector> vectors;
    private final List<Accessor> accessors;
    private final List<AccountRelation> relationships;
    private final long ifiID;
    private final String ownerAccountHolderID;
    private final String accountProviderID;
    private final Long cardID;
    private final String name;
    private final long productFamilyID;
    private final long productID;
    private final List<String> programIDs;
    private final long ledgerID;
    private final String status;
    private final List<Tag> tags;
    private final Map<String, String> attributes;
    private final LocalDateTime createdAt;
    private final LocalDateTime updatedAt;
}
