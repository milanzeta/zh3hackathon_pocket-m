package com.example.bookstore.models.response;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class Tag {
    private final String type;
    private final String value;
    private final Map<String, String> attributes = new HashMap();
}