package com.example.bookstore.models.response;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Accessor {
    private final String id;
    private final Long ifiID;
    private final String accountHolderID;
    private final String accountID;
    private final List<String> transactionPolicyIDs;
    private final String status;
    private final Map<String, String> attributes;
    private final Timestamp createdAt;
    private final Timestamp updatedAt;
}