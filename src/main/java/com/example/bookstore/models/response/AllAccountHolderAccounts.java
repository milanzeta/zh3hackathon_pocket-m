package com.example.bookstore.models.response;

import lombok.Data;

import java.util.List;

@Data
public class AllAccountHolderAccounts {
    List<AccountSheet> accounts;
}
