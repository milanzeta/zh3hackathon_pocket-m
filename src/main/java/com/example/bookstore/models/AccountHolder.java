package com.example.bookstore.models;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class AccountHolder {
    private String accountHolderId;
}
