package com.example.bookstore.models;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Builder
@Data
public class CardSku {
    private String cardSkuId;
    private String productID;
    private String ifi;
    private String bin;
    private String plasticCode;
    private String vendorID;
    private List<String> tags;
    private String range;
}
