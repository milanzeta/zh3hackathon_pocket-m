package com.example.bookstore.models.request;

import lombok.Builder;
import lombok.Data;

import java.util.Map;

@Data
@Builder
public class FundsTransferRequest {
	private Amount amount;
	private Long transferTime;
	private String requestID;
	private String debitAccountID;
	private Map<String,String> attributes;
	private String creditAccountID;
	private String transferCode;
	private String remarks;
}
