package com.example.bookstore.models.request;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Amount {
	private Double amount;
	private String currency;
}
