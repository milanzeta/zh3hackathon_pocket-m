package com.example.bookstore.models.request;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class TransferFundsRequest {
    private String fromAccountHolderId;
    private String toAccountHolderId;
    private int amount;
}
