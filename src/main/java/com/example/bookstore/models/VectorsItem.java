package com.example.bookstore.models;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class VectorsItem{
	private String type;
	private String value;
}
