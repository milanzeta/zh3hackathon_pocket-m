package com.example.bookstore.models;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class Success {
    Boolean success;
}
