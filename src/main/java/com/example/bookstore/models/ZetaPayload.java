package com.example.bookstore.models;

import javax.annotation.Nullable;

import javax.validation.Payload;
import java.util.HashMap;
import java.util.Map;

public abstract class ZetaPayload implements Payload {
    private final Map<String, String> headers;

    protected ZetaPayload() {
        this(new HashMap());
    }

    protected ZetaPayload(Map<String, String> headers) {
        this.headers = headers;
    }

    protected void setHeader(String name, String value) {
        this.headers.put(name, value);
    }

    public Map<String, String> getHeaders() {
        return this.headers;
    }

    @Nullable
    public String getHeader(String key) {
        return this.headers.containsKey(key) ? (String)this.headers.get(key) : null;
    }
}
