package com.example.bookstore.models;

import lombok.Builder;
import lombok.Data;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Builder
@Data
public class FormFactor extends ZetaPayload {
    private String id;
    private Long ifi;
    private String formFactorProductID;
    private String formFactorID;
    private String identityID;
    private String targetURI;
    private List<String> tags;
    private Map<String, Object> attributes;
    private Policies policies;
    private Status status;
    private StatusReason reason;
    private Date createdAt;
    private Date modifiedAt;
}