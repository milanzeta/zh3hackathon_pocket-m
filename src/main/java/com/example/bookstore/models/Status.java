package com.example.bookstore.models;


public enum Status {
    ACTIVE("ACTIVE"),
    INACTIVE("INACTIVE"),
    DELETED("DELETED");

    private String status;

    Status(String status) {
        this.status = status;
    }
}
