package com.example.bookstore.models;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class AccountHolderCreationResponse {
    String accountHolderId;
    Integer limit;
    String name;
    String profilePicURL;
    int maxLimit;
}
