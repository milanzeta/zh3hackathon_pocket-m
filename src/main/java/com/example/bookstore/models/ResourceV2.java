package com.example.bookstore.models;

import lombok.Builder;
import lombok.Getter;

import java.util.Map;

@Builder
@Getter
public class ResourceV2 {
    private String uri;
    private Map<String, String> attributes;
}
