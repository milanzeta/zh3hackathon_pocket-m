package com.example.bookstore.models;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class Authorization {
    String phoneNumber;
    String password;
    String accountHolderId;
    int money_limit;
    String username;
    String profilePicURL;


}
