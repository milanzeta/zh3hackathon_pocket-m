package com.example.bookstore.models;

import lombok.Builder;
import lombok.Data;

import java.util.Date;
import java.util.Map;

@Builder
@Data
public class OrderDetails {
    private String orderID;
    private String cardSkuID;
    private CardSku cardSku;
    private String plasticCode;
    private String thirdLineEmbossing;
    private String fourthLineEmbossing;
    private Map<String, String> expiry;
    private Map<String, String> deliveryAddress;
    private Map<String, String> tenantAttributes;
    private Map<String, String> vendorAttributes;
    private String orderStatus;
    private Date embossedAt;
    private Date orderedAt;
}
