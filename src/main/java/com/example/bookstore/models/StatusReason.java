package com.example.bookstore.models;

public class StatusReason {
    private StatusReasonCode code;
    private String description;

    public StatusReasonCode getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

}
