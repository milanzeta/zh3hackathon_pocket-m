package com.example.bookstore.models;

import lombok.Builder;
import lombok.Getter;

import java.util.Date;
import java.util.Map;

@Builder
@Getter
public class CardViewResponse {
    private AccountV2 account;
    private ResourceV2 resource;
    private String cardID;
    private String crn;
    private String cardType;
    private String maskedPan;
    private String cardStatus;
    private Date associatedAt;
    private OrderDetails orderDetails;
    private Map<String, String> tenantAttributes;
    private CardBinRange binRange;
    private transient long ifi;
    private SensitiveCardView sensitiveView;
}