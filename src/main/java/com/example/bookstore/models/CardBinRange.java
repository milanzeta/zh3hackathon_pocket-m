package com.example.bookstore.models;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class CardBinRange {
    private String bin;
    private String range;
}

