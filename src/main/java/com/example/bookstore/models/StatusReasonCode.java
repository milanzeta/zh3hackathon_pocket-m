package com.example.bookstore.models;

public enum StatusReasonCode {
    LOST_OR_STOLEN("LOST_OR_STOLEN"),
    DAMAGED("DAMAGED"),
    HOTLISTED("HOTLISTED"),
    OTHER("OTHER");

    private String status;

    StatusReasonCode(String status) {
        this.status = status;
    }
}

