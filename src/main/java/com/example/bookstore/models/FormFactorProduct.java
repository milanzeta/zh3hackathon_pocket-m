package com.example.bookstore.models;

import lombok.Builder;
import lombok.Data;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Builder
@Data
public class FormFactorProduct extends ZetaPayload {
    private Long ifi;
    private String id;
    private String code;
    private String name;
    private String description;
    private String type;
    private Policies policies;
    private String provider;
    private String skuID;
    private List<String> tags;
    private Map<String, Object> attributes;
    private Status issuanceStatus;
    private Status paymentStatus;
    private Date createdAt;
    private Date modifiedAt;
}