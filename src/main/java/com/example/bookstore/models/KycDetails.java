package com.example.bookstore.models;

import lombok.Builder;
import lombok.Data;

import java.util.Map;

@Builder
@Data
public class KycDetails{
	private String kycStatus;
	private String kycStatusPostExpiry;
	private Map<String,String> authData;
	private String authType;
}
