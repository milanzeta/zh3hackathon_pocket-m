package com.example.bookstore.models;

public class ECDHEncryptedCardPayload {
    private String serverPublicKey;
    private String encryptedData;
    private String iv;
}
