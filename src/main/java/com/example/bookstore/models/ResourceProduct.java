package com.example.bookstore.models;

import lombok.Builder;
import lombok.Data;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Builder
@Data
public class ResourceProduct extends ZetaPayload {
    private Long ifi;
    private String id;
    private String code;
    private String name;
    private String description;
    private List<FormFactorProduct> formFactorProducts;
    private List<String> tags;
    private Policies policies;
    private Map<String, Object> attributes;
    private Status status;
    private Date createdAt;
    private Date modifiedAt;
}