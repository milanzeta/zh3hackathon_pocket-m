package com.example.bookstore.models;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class IssueBundleReq {
	private String accountHolderID;
	private String phoneNumber;
	private String name;
}
