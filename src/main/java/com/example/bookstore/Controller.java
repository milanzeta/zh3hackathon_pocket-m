package com.example.bookstore;
import com.example.bookstore.models.*;
import com.example.bookstore.models.request.TransferFundsRequest;
import com.example.bookstore.models.response.Balance;
import com.example.bookstore.models.response.TransactionList;
import com.example.bookstore.service.AccountHolderService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.CompletionStage;

@RestController
@RequestMapping("api")
public class Controller {
    private final AccountHolderService accountHolderService;

    public Controller(AccountHolderService accountHolderService) {
        this.accountHolderService = accountHolderService;
    }

    @PostMapping("/accountHolder")
    @CrossOrigin
    public CompletionStage<AccountHolderCreationResponse> createMinKycAccountHolder(@RequestBody CreateMinKycAccountHolderRequest request) {
        return accountHolderService.createMinKYCAccountHolder(request);
    }

    @PostMapping("/login")
    @CrossOrigin
    public CompletionStage<AccountHolderCreationResponse> login(@RequestBody LoginRequest loginRequest) {
        return accountHolderService.login(loginRequest);
    }

    @PostMapping("/getCard")
    @CrossOrigin
    public CompletionStage<CardViewResponse> getCard(@RequestBody AccountHolder accountHolder) {
        System.out.println(accountHolder.getAccountHolderId());
        return accountHolderService.getCardView(accountHolder.getAccountHolderId());
    }

    @PostMapping("/transferMoney")
    @CrossOrigin
    public CompletionStage<AccountHolderCreationResponse> transferMoney(@RequestBody TransferFundsRequest transferFundsRequest) {
        System.out.println("from : " + transferFundsRequest.getFromAccountHolderId());
        System.out.println("to : " + transferFundsRequest.getToAccountHolderId());
        return accountHolderService.transferMoney(transferFundsRequest);
    }

    @GetMapping("/accountHolder/{accountHolderID}/accounts")
    @CrossOrigin
    public CompletionStage<Balance> getBalance(@PathVariable("accountHolderID") String accountHolderID) {
        return accountHolderService.fetchAllAccountsBalance(accountHolderID);
    }

    @GetMapping("/accountHolder/{accountHolderID}/accounts/transactions")
    @CrossOrigin
    public TransactionList getAllTransactionsOfAnAccountHolder(@PathVariable("accountHolderID") String accountHolderID,
                                                               @RequestParam("pageNumber") String pageNumber,
                                                               @RequestParam("pageSize") String pageSize) {
        return accountHolderService.getAllTransactionsForSingleAccount(accountHolderID);
    }
}